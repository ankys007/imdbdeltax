﻿using System;
using System.Collections.Generic;

namespace Imdbapp.domain
{
    public class Movie
    {
        public string Name
        {
            get;
            set;
        }
        public int YearOfRelease
        {
            get;
            set;
        }
        public string Plot
        {
            get;
            set;
        }

        public List<Actor>Actor
        {
            get;
            set;
        }
        public Producer Producer
        {
            get;
            set;
        }
        public int Id
        {
            get;
            set;
        }
    }
}