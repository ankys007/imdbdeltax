﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imdbapp.domain
{
    public class Person
    {
        public string Name
        {
            get;
            set;
        }

        public int Id
        {
            get;
            set;
        }

        public string DateOfBirth
        {
            get;
            set;
        }

    }
   public class Actor: Person
    {

    }
    public class Producer : Person
    {

    }
}

