﻿
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using System.Linq;
using IMDB_APP;
using Imdbapp.domain;
using System.Collections.Generic;

namespace Imdbapp.tests
{
    [Binding]
    public class ImdbAppSteps
    {
        private string _movieName;

        private string _releaseYear;

        private string _plot;

        private string _actorIds;

        private int _movieId;

        private string _actorId;

        private string _prodId;

        private string _producerId;

        private readonly MovieService _movieService;

        private readonly ActorService _actorService;

        private readonly ProducerService _producerService;



        public ImdbAppSteps()
        {
            _actorService = new ActorService();
            _producerService = new ProducerService();
            _movieService = new MovieService(_actorService, _producerService);
        }

        [Given(@"movie's name is ""(.*)""")]
        public void GivenMovieSNameIs(string movieName)
        {
            _movieName = movieName;
        }

        [Given(@"the year of release of the movie is ""(.*)""")]
        public void GivenTheYearOfReleaseOfTheMovieIs(string releaseYear)
        {
            _releaseYear = releaseYear;
        }

        [Given(@"the plot of the movie is ""(.*)""")]
        public void GivenThePlotOfTheMovieIs(string plot)
        {
            _plot = plot;
        }

        [Given(@"the indexes of the actors are ""(.*)""")]
        public void GivenTheIndexesOfTheActorsAre(string actorIds)
        {
            _actorIds = actorIds;
        }

        [Given(@"We have all movies in the moviestore")]
        public void GivenWeHaveAllMoviesInTheMoviestore()
        {

        }
        [Given(@"the actor id is ""(.*)""")]
        public void GivenTheActorIdIs(string actorId)
        {
            _actorId = actorId;
        }

        [Given(@"the producer id is ""(.*)""")]
        public void TheProducerIdIs(string prodId)
        {
            _prodId = prodId;
        }

        [Given(@"the index of the producer is ""(.*)""")]
        public void GivenTheIndexOfTheProducerIs(string producerId)
        {
            _producerId = producerId;
        }

        [When(@"add the movie to the moviestore")]
        public void WhenAddTheMovieToTheMoviestore()
        {
            _movieId = _movieService.Add(_movieName, _releaseYear, _plot, _actorIds, _producerId);
        }

        [BeforeScenario(new string[] { "add-movie", "get-all-movies", "get-all-movies-using-actor-id","delete-the-producer"})]
        public void AddActors()
        {
            _actorService.Add("RDJ", "09/10/1970");
            _actorService.Add("Chris Evans", "25/10/1998");
        }

        [BeforeScenario(new string[] { "add-movie", "get-all-movies", "get-all-movies-using-actor-id"})]
        public void AddProducer()
        {
            _producerService.Add("Kevin Feige", "02/07/1998");
        }
        [BeforeScenario("delete-the-producer")]
        public void AddTheProducers()
        {
            _producerService.Add("Kevin Feige", "02/07/1998");
            _producerService.Add("George Martin", "04/10/1999");
        }
        
        [BeforeScenario(new string[] { "get-all-movies", "get-all-movies-using-actor-id", "delete-the-producer" })]
        public void AddMovie()
        {
            _movieId = _movieService.Add("Avengers", "2009", "Loki gains the power of Tesseract", "1,2", "1");
        }

        [When(@"Fetch all the movies from the moviestore")]
        public void WhenFetchAllTheMoviesFromTheMoviestore()
        {
            _movieService.Get();
        }

        [When(@"Get all the movies for the actorid")]
        public void GetAllTheMoviesForTheActorId()
        {
            _movieService.GetUsingActorId(_actorId);
        }

        [When(@"Delete the producer")]
        public void DeleteTheProducer()
        {
            _producerService.Delete(_prodId, _movieService.GetUsingProducerId(_prodId));
        }

        [Then(@"the movie list of the actor should look like")]
        public void ThenTheMovieListOfTheActorShouldLookLike(Table table)
        {
            var res = _movieService.GetUsingActorId(_actorId).Where(x => x.Id == _movieId);
            table.CompareToSet(res);
        }

        [Then(@"the movie list should look like")]
        public void ThenTheMovieListShouldLookLike(Table table)
        {
            var res = _movieService.Get().Where(x => x.Id == _movieId);
            table.CompareToSet(res);
        }

        [Then(@"the actor list should look like")]
        public void ThenTheActorListShouldLookLike(Table table)
        {
            // var res = _movieService._actorService.GetEntity().Where(x => x.Id == _movieId);
            var res = _actorService.Get();
            table.CompareToSet(res);

        }

        [Then(@"the producer list should look like")]
        public void ThenTheProducerListShouldLookLike(Table table)
        {
            //var res = _movieService.Get().Where(x => x.Id == _movieId);
            var res = _producerService.Get();
            table.CompareToSet(res);
        }

       



    }
}
