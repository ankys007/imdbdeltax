﻿Feature: Imdb App
	This app manages everything related to movies

@add-movie
Scenario: Add movie to the non-empty moviestore
	Given movie's name is "Avengers"
	And the year of release of the movie is "2009"
	And the plot of the movie is "Loki gains the power of Tesseract"
	And the indexes of the actors are "1,2"
	And the index of the producer is "1"
	When add the movie to the moviestore
	Then the movie list should look like
		| Name     | YearOfRelease | Plot                              |
		| Avengers | 2009          | Loki gains the power of Tesseract |
	Then the actor list should look like
		| Name        | DateOfBirth |
		| RDJ         | 09/10/1970  |
		| Chris Evans | 25/10/1998  |
	Then the producer list should look like
		| Name          | DateOfBirth |
		| Kevin Feige   | 02/07/1998  |

@get-all-movies
Scenario: Get all movies in the library
	Given We have all movies in the moviestore
	When Fetch all the movies from the moviestore
	Then the movie list should look like
		| Name     | YearOfRelease | Plot                              |
		| Avengers | 2009          | Loki gains the power of Tesseract |
	Then the actor list should look like
		| Name        | DateOfBirth |
		| RDJ         | 09/10/1970  |
		| Chris Evans | 25/10/1998  |
	Then the producer list should look like
		| Name          | DateOfBirth |
		| Kevin Feige   | 02/07/1998  |

@get-all-movies-using-actor-id
Scenario:Get all movies using actor id
	Given the actor id is "1"
	When Get all the movies for the actorid
	Then the movie list of the actor should look like
		| Name     | YearOfRelease | Plot                              |
		| Avengers | 2009          | Loki gains the power of Tesseract |

@delete-the-producer
Scenario:Delete the producer
	Given the producer id is "2"
	When Delete the producer
	Then the producer list should look like
		| Name        | DateOfBirth |
		| Kevin Feige | 02/07/1998  |