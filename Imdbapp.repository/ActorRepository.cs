﻿
using System.Collections.Generic;
using Imdbapp.domain;
using System.Linq;
namespace Imdbapp.repository
{
    public class ActorRepository
    {
        private readonly List<Actor> _actors;

        public ActorRepository()
        {
            _actors = new List<Actor>();
        }
        public void Add(Actor actor)
        {
            _actors.Add(actor);
        }
        public List<Actor> Get()
        {
            return _actors.ToList();
        }
        public void Delete(Actor actor)
        {
            _actors.Remove(actor);
        }


    }
}