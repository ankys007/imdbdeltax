﻿using System;
using System.Linq;
using System.Collections.Generic;
using Imdbapp.domain;


namespace Imdbapp.repository
{
    public class MovieRepository
    {
        private List<Movie> _movies;

        public MovieRepository()
        {
            _movies = new List<Movie>();
        }
        public void Add(Movie movie)
        {
            _movies.Add(movie);
        }
        public List<Movie> Get()
        {
            return _movies.ToList();
        }
        public void Delete(Movie movie)
        {
          _movies.Remove(movie);
        }
    }
}
