﻿using System;
using System.Collections.Generic;
using System.Text;
using Imdbapp.domain;
using System.Linq;

namespace Imdbapp.repository
{
    public class ProducerRepository
    {
        private  readonly List<Producer> _producers;

        public ProducerRepository()
        {
            _producers = new List<Producer>();
        }
        public void Add(Producer producer)
        {
            _producers.Add(producer);
        }
        public List<Producer> Get()
        {
            return _producers.ToList();
        }
        public void Delete(Producer producer)
        {
            _producers.Remove(producer);
        }
    }
}