﻿using IMDB_APP;
using Imdbapp.domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace imdbapp1
{
    class Program
    {

        static void DisplayMenu()
        {
            Console.WriteLine(" 1. List Movies");
            Console.WriteLine(" 2. Add the movie");
            Console.WriteLine(" 3. Add the actor");
            Console.WriteLine(" 4. Add the producer");
            Console.WriteLine(" 5. Delete the movie");
            Console.WriteLine(" 6. List the movies acted by the given actor");
            Console.WriteLine(" 7. Remove the actor");
            Console.WriteLine(" 8. List the movies produced by the given producer");
            Console.WriteLine(" 9. Remove the producer");
            Console.WriteLine(" 10. Exit");

        }
        static void Main(string[] args)
        {
            var actorService = new ActorService();
            var producerService = new ProducerService();
            var movieService = new MovieService(actorService, producerService);
            try
            {
                string choice;

                DisplayMenu();
                Console.WriteLine("Enter Choice");
                choice = Console.ReadLine();
                while (choice != "10")
                {
                    switch (choice)
                    {
                        case "1":
                            List<Movie> MovieList = movieService.Get();
                            foreach (var entity in MovieList)
                            {
                                Console.WriteLine($"The moviename is {entity.Name}");
                                Console.WriteLine($"The movie's year of release is {entity.YearOfRelease}");
                                Console.WriteLine($"The plot is {entity.Plot}");
                                Console.WriteLine($"The actors are {string.Join(",", entity.Actor.Select(x => x.Name))}");
                                Console.WriteLine($"The producer is {entity.Producer.Name}");
                            }

                            break;

                        case "2":
                            Console.WriteLine("Enter the name of the movie");
                            var movieName = Console.ReadLine();

                            Console.WriteLine("Enter the year of release");
                            var yearOfRelease = Console.ReadLine();

                            Console.WriteLine("Enter the plot");
                            var plotOfMovie = Console.ReadLine();

                            Console.WriteLine("Choose the actors id's from below with comma separated");
                            List<Actor> actors = actorService.Get();
                            foreach (var act in actors)
                            {
                                Console.WriteLine($"{act.Id}.{act.Name}");
                            }
                            string actorIds = Console.ReadLine();

                            Console.WriteLine("Choose the producer id,you can only choose one");
                            List<Producer> producers = producerService.Get();
                            foreach (var producer in producers)
                            {
                                Console.WriteLine($"{producer.Id}.{producer.Name}");
                            }
                            string producerId = Console.ReadLine();
                            movieService.Add(movieName, yearOfRelease, plotOfMovie, actorIds, producerId);
                            break;

                        case "3":
                            Console.WriteLine("Enter the name of the actor");
                            var actorName = Console.ReadLine();
                            Console.WriteLine("Enter actor's DOB(DD/MM/YYYY): ");
                            string actorDOB = Console.ReadLine();
                            actorService.Add(actorName, actorDOB);
                            Console.WriteLine("Actor added Successfully");
                            break;

                        case "4":
                            Console.WriteLine("Enter the name of the producer");
                            var producerName = Console.ReadLine();
                            Console.WriteLine("Enter producer's DOB(DD/MM/YYYY): ");
                            string producerDOB = Console.ReadLine();
                            producerService.Add(producerName, producerDOB);
                            Console.WriteLine("Producer added Successfully");
                            break;

                        case "5":
                            Console.WriteLine("The movies are listed below");
                            List<Movie> movies = movieService.Get();

                            foreach (var entity in movies)
                            {
                                Console.WriteLine($"{entity.Id}.{entity.Name}");
                            }

                            Console.WriteLine("Enter the movieid to delete");
                            var movieId = Console.ReadLine();

                            movieService.Delete(movieId);
                            Console.WriteLine("Movie has been deleted successfully");


                            break;
                        case "6":
                            Console.WriteLine("Enter the actorid to see in which movies the actor has worked");
                            var actorId = Console.ReadLine();

                            Console.WriteLine("The actor with the given id has worked in the following movies");
                            var actorMovies = movieService.GetUsingActorId(actorId);
                            foreach (var movie in actorMovies)
                            {
                                Console.WriteLine($"{movie.Name}");
                            }


                            break;
                        case "7":
                            Console.WriteLine("Enter the actor id you want to delete");
                            var id = Console.ReadLine();
                            actorService.Delete(id, movieService.GetUsingActorId(id));
                            Console.WriteLine("The actor has been removed successfully");


                            break;
                        case "8":
                            Console.WriteLine("Enter the producerid to see in which movies the actor has worked");
                            var prodId = Console.ReadLine();

                            Console.WriteLine("The actor with the given id has worked in the following movies");
                            var producerMovies = movieService.GetUsingProducerId(prodId);
                            foreach (var movie in producerMovies)
                            {
                                Console.WriteLine($"{movie.Name}");
                            }

                            break;

                        case "9":
                            Console.WriteLine("Enter the producer id you want to delete");
                            var proId = Console.ReadLine();
                            producerService.Delete(proId, movieService.GetUsingProducerId(proId));
                            Console.WriteLine("The producer has been removed successfully");

                            break;

                        default:
                            {
                                Console.WriteLine("Wrong Choice,please re-enter choice");
                                break;
                            }
                    }
                    Console.WriteLine("Enter Choice");
                    choice = Console.ReadLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

    }
}