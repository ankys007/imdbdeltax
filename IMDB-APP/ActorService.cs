﻿using Imdbapp.domain;
using Imdbapp.repository;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace IMDB_APP
{
    public class ActorService
    {
        private readonly ActorRepository _actorRepository;
        public ActorService()
        {
            _actorRepository = new ActorRepository();
            //_movieService = new MovieService(_actorService,_producerService);
        }

        public void Add(string name, string dateOfBirth)
        {
            DateTime actorDateOfBirth;
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new Exception("Actor name is required");
            }
            if (string.IsNullOrWhiteSpace(dateOfBirth))
            {
                throw new Exception("Actor date of birth is required");
            }
            if (DateTime.TryParseExact(dateOfBirth, "dd/MM/yyyy", null, DateTimeStyles.None, out DateTime parseDateOfBirth))
            {
                actorDateOfBirth = parseDateOfBirth;

            }
            else
            {
                throw new Exception("Invalid date format and the format should be dd/MM/yyyy");
            }
            var actorId = _actorRepository.Get().Count + 1;
            var actor = new Actor()
            {
                Id = actorId,
                Name = name,
                DateOfBirth = actorDateOfBirth.ToString("dd/MM/yyyy")
            };
            _actorRepository.Add(actor);
        }

        public List<Actor> Get()
        {
            var actors = _actorRepository.Get();
            if (actors.Count <= 0)
            {
                throw new Exception("There are no actors");
            }
            return actors;
        }

        public Actor GetEntity(int id)
        {
            if (id < 0)
            {
                throw new Exception($"Invalid actor id is{id}"); ;
            }
            var actor = Get().FirstOrDefault(act => act.Id == id);
            if (actor == null)
            {
                throw new Exception($"The actor doesn't exist for the given id is{id}");
            }

            return actor;
        }

        public void Delete(string id, List<Movie> movies)
        {
            int parsedActorId = int.Parse(id);
            var actor = Get().FirstOrDefault(act => act.Id == parsedActorId);

            if (movies.Count > 0)
            {
                throw new Exception($"The actor with id {id} is associated with some movie,can't remove it");
            }
            _actorRepository.Delete(actor);
        }
    }


}


