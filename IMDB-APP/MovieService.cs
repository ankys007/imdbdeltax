﻿using System;
using System.Collections.Generic;
using System.Linq;
using Imdbapp.domain;
using Imdbapp.repository;

namespace IMDB_APP
{
    public class MovieService
    {
        private readonly ActorService _actorService;
        private readonly ProducerService _producerService;
        private readonly MovieRepository _movieRepository;

        public MovieService(ActorService actorService, ProducerService producerService)
        {
            _actorService = actorService;
            _producerService = producerService;
            _movieRepository = new MovieRepository();

        }
        public int Add(string name, string yearOfRelease, string plot, string actorIds, string producerId)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new Exception("Movie name is required");
            }
            if (string.IsNullOrWhiteSpace(yearOfRelease))
            {
                throw new Exception("Year of Release is required");
            }
            if (string.IsNullOrWhiteSpace(plot))
            {
                throw new Exception("plot is required");
            }
            if (string.IsNullOrWhiteSpace(actorIds))
            {
                throw new Exception("actorId's is required");
            }
            if (string.IsNullOrWhiteSpace(producerId))
            {
                throw new Exception("producerId is required");
            }
            var movieYearOfRelease = 0;
            if (int.TryParse(yearOfRelease, out int parsedYearOfRelease))
            {
                movieYearOfRelease = parsedYearOfRelease;
            }
            else
            {
                throw new Exception($"Invalid movie year of release{yearOfRelease}");
            }

            List<Actor> actors = new List<Actor>();
            foreach (var id in actorIds.Split(","))
            {
                if (int.TryParse(id, out int parsedActorId))
                {
                    actors.Add(_actorService.GetEntity(parsedActorId));
                }
                else
                {
                    throw new Exception($"Invalid actorId ${actorIds}");
                }
            }
            Producer producer = new Producer();
            if (int.TryParse(producerId, out int parsedProducerId))
            {
                producer = _producerService.GetEntity(parsedProducerId);
            }



            var movieId = _movieRepository.Get().Count + 1;
            var movie = new Movie()
            {
                Name = name,
                YearOfRelease = movieYearOfRelease,
                Plot = plot,
                Actor = actors,
                Producer = producer,
                Id = movieId
            };

            _movieRepository.Add(movie);
            return movieId;



        }
        public List<Movie> Get()
        {
            var movies = _movieRepository.Get();
            if (movies.Count <= 0)
            {
                throw new Exception("There are no movies");
            }
            return movies;
        }

        public void Delete(string id)
        {
            if (int.TryParse(id, out int parsedMovieId))
            {
                if (parsedMovieId < 0)
                {
                    throw new Exception($"Invalid movie id {parsedMovieId}");
                }
                var movie = Get().FirstOrDefault(x => x.Id == parsedMovieId);
                if (movie == null)
                {
                    throw new Exception($"Movie doesn't exist for the given id {parsedMovieId}");
                }

                _movieRepository.Delete(movie);
            }
            throw new Exception($"Invalid movie id {id}");

        }
        public List<Movie> GetUsingActorId(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new Exception("actorId is required");
            }
            if (int.TryParse(id, out int parsedActorId))
            {
                var actor = _actorService.GetEntity(parsedActorId);
            }
            List<Movie> movieList = new List<Movie>();
            List<Movie> movies = Get();
            movieList.AddRange(movies.SelectMany(movie => movie.Actor.Where(actor => actor.Id == parsedActorId).Select(actor => movie)));
          
            return movieList;
        }
           
        public List<Movie> GetUsingProducerId(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new Exception("producerId is required");
            }
            if (int.TryParse(id, out int parsedProducerId))
            {
                var producer = _producerService.GetEntity(parsedProducerId); 
            }
            List<Movie> movieList = new List<Movie>();
            List<Movie> movies = Get();
            movieList.AddRange(movies.Where(movie => movie.Producer.Id == parsedProducerId));
            return movieList;
        }




    }
}
