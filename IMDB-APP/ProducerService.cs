﻿using Imdbapp.domain;
using Imdbapp.repository;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace IMDB_APP
{
    public class ProducerService
    {
        private  ProducerRepository _producerRepository;

        public ProducerService()
        {
            _producerRepository = new ProducerRepository();
        }
        public void Add(string name, string dateOfBirth)
        {
            DateTime producerDateOfBirth;
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new Exception("Producer name is required");
            }
            if (string.IsNullOrWhiteSpace(dateOfBirth))
            {
                throw new Exception("Producer date of birth is required");
            }
            if (DateTime.TryParseExact(dateOfBirth, "dd/MM/yyyy", null, DateTimeStyles.None, out DateTime parseDateOfBirth))
            {
                producerDateOfBirth = parseDateOfBirth;

            }
            else
            {
                throw new Exception("Invalid date format and the format should be dd/MM/yyyy");
            }
            var producerId = _producerRepository.Get().Count + 1;

            var producer = new Producer()
            {
                Id = producerId,
                Name = name,
                DateOfBirth =producerDateOfBirth.ToString("dd/MM/yyyy")
            };
            _producerRepository.Add(producer);
        }
        public List<Producer> Get()
        {
            var producers= _producerRepository.Get();
            if(producers.Count<=0)
            {
                throw new Exception("There are no producers");
            }
            return producers;

        }
        public Producer GetEntity(int id)
        {
            if (id < 0)
            {
                throw new Exception($"Invalid producer id is{id}"); ;
            }
            var producer= Get().FirstOrDefault(prod => prod.Id == id);
            if(producer==null)
            {
                throw new Exception($"Producer doesn't exist for the given {id}");
            }
            return producer;
        }
        public void Delete(string id, List<Movie> movies)
        {
            int parsedProducerId = int.Parse(id);
            var producer = Get().FirstOrDefault(prod => prod.Id == parsedProducerId);

            if (movies.Count > 0)
            {
                throw new Exception($"The producer with id {id} is associated with some movie,can't remove it");
            }
            _producerRepository.Delete(producer);
        }
    }
}
